\documentclass{article}
\usepackage[utf8]{inputenc}

\title{MAT267 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{January 8 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}
\newtheorem*{corollary}{Corollary}
\newtheorem{exercise}{Exercise}
\newtheorem{claim}{Claim}

\DeclareMathOperator{\Int}{Int}
\DeclareMathOperator{\grad}{grad}
\DeclareMathOperator{\Ker}{Ker}
\DeclareMathOperator{\Ima}{Im}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\prt}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\hlfspc}[0]{\mathbb{H}}
\newcommand{\loint}[0]{\operatorname{L}\int}
\newcommand{\hiint}[0]{\operatorname{U}\int}
\newcommand{\indic}[1]{\chi_{#1}}

\begin{document}

\maketitle

Let's start with some examples of an equation. \(x^2 + x = 3\) is an equation, as is \(x^2 - xy = 0\). The former has solutions \(x = \pm 3 \in \ints\), and hence in \(\rationals, \reals, \mathbb{C}\) and the quaternions, \(\mathbb{H}\). In general, with simple equations like this, we're looking for a solution which is some kind of number, and there's a hierarchy of sets in which this solution may live.

Now let's look at an example of a differential equation. In a differential equation, the unknown is a \underline{function}. An example of a differential equation is
\begin{equation}
  x^2y' + y^2 = \ln(x)
  \label{ordif}
\end{equation}
The unknown here is the \textit{function} \(y(x)\). We can also write differential equations like
\begin{equation}
  \prt{u}{t} = 3\prt{^2u}{x^2}
  \label{partialdif}
\end{equation}
with the unknown being \(u(x, t)\). Or we can write equations without any unknown variable, like
\begin{equation}
  y' = 3y
  \label{nodepn}
\end{equation}
Here the unknown is the function \(y\), which can have whatever dependent variable we want, like \(y(x), y(t)\) or even \(y(happyface)\).

Equations \ref{ordif} and \ref{nodepn} are \textit{ordinary differential equations} since they use regular, first-year derivatives, whereas \ref{partialdif} is a \textit{partial differential equation}, using partial derivatives. We also have \textit{integral equations}, like
\begin{equation}
  \phi(x) = 3\int_{-1}^1e^{x - y}\phi(y)dy
\end{equation}
Here, the unknown is \(\phi(x)\). All these types of differential equations are more complicated than the ordinary equations we learned how to solve in elementary school, and in this course we'll be concerned with ordinary differential equations, or \textbf{ODE}s.

Now, you may know from studying or TAing calculus that if you write down a random integral, chances are you won't be able to solve it directly. The same thing applies to differential equations: equations that you can solve are quite special, and part of this course is learning to recognize them. So let's work with an example:
\begin{equation}
  y' = 3y
  \label{simpleode}
\end{equation}
One solution is the trivial solution, \(y = 0\). Another solution is
\begin{equation}
  \forall c \in \mathbb{C}, y(x) = ce^{3x}
\end{equation}
Note we could write this with \(s, t\) or any other variable, and note that we can have complex valued solutions. That said, we will be working mainly with \(\reals\) in this course. One example of a multivariable differential equation is
\begin{equation}
  y(x, t) = \cos(t)e^{3x}
\end{equation}
Note that it's confusing here which variable we're using, or perhaps if we're using both. One of the things people do in differential equations papers is define exactly what they mean by a solution. Alternatively, you could be more precise, for example, by using notation of the form \(Df\), but usually textbook authors just expect you to acclimate to whatever sloppy notation is being used today.

Let's now get precise ourselves about the meaning of a solution:
\begin{definition}
  \(y(x)\) with domain \((a, b)\) is a \underline{solution} of equation \ref{simpleode} if \(y\) is differentiable on \((a, b)\) and satisfies
  \begin{equation}
    \forall x \in (a, b), \frac{dy}{dx}(x) = 3y(x)
  \end{equation}
\end{definition}
Note how we talk about solutions over an \textit{interval}.
\begin{equation}
  y = ce^{3x}, x \in \reals, c in \reals
\end{equation}
is the general (real-valued) solution of \(y' = 3y\). The total set of solutions, then, is
\begin{equation}
  \bigcup_{c \in \reals}ce^{3x}
\end{equation}
How can we pick out a solution? One way is to ask for a constraint, like that the solution goes through a particular point. For example, if we ask that the solution goes through \((x_0, y_0)\), we can write
\begin{equation}
  ce^{3x_0} = y_0 \impliedby y(x) = y_0e^{3(x - x_0)}
\end{equation}
Now let's think about a more complicated example:
\begin{equation}
  y' = -\frac{x}{y}
  \label{morecomp}
\end{equation}
Here, we seek solutions \(y(x)\) with domain... I don't know what the domain is going to be yet. On the other hand, looking at this, there's some ``danger Will Robinson'' situations. Where can things go bad? \(y = 0\), of course.

One way of looking at is is that at \(y = 0\), the slope becomes infinite. Another way is that when \(y = 0\),
\begin{equation}
  yy' = -x
  \label{otherform}
\end{equation}
The way we're going to tackle this is by doing formal manipulations: doing ``whatever we want'' with the equation, whatever ``feels right'', and then, once we're done, proving our result is correct, versus worrying about whether our steps arejustified. So let's try manipulating Equation \ref{otherform}, to get
\begin{equation}
  y(x)y'(x) = \left(\frac{1}{2}y^2(x)\right) = -x
\end{equation}
\textit{suggesting}
\begin{equation}
  \frac{1}{2}y^2(x) = -\frac{1}{2}x^2 + c, c \in \reals
\end{equation}
Looking at this, we can rewrite it to
\begin{equation}
  \frac{1}{2}y^2(x) + \frac{1}{2}x^2 = c
\end{equation}
If we're working in \(\reals\), this means we must restrict \(c\) to positive or zero numbers. We shouldn't just restrict everything to \(c = 0\) though, because then we'll just get \(x, y = 0\), which doesn't even really solve our \textit{original} equation \ref{morecomp}. So we have \(c \in \reals^+\).

Multiplying by 2 and ``swallowing'' into the constant, we get
\begin{equation}
  y(x)^2 = c - x^2, c \in \reals^+
\end{equation}
giving the result of our \textit{formal} manipulations on
\begin{equation}
\begin{split}
  y_+(x) = \sqrt{c - x^2}, x \in (-\sqrt{c}, \sqrt{c}) \\
  y_-(x) = -\sqrt{c - x^2}, x \in (-\sqrt{c}, \sqrt{c}) \\
  c \in \reals^+
\end{split}
\end{equation}
Once we have this, we need to check that it actually solves the differential equation. It is differentiable, and it does, so this is the moment we go from mere formal manipulations to truth. This \textit{is} the ``general'' solution of \(y' = -\frac{x}{y}\).

We can leave the intervals \((-\sqrt{c}, \sqrt{c})\) if we're willing to have complex values solution. We know that this set of solutions fills up the whole plane except the \(x\)-axis, since if we pick any point, we can find a solution through it. On the other hand, I put ``general'' in quotation marks since we don't know that these solutions through points are \textit{unique}. We usually say straight up general for linear systems, where we \textit{can} be certain about uniqueness, but we don't always get it for free. One of the million-dollar Millenium Problems relates to whether solutions to the Navier-Stokes equation are unique. You may say \textit{a} general solution in cases like the above, but I'll just say general.

This does give us an interesting question, which is, so we have these solutions, and, if I asked you for the solution that went through the point \((1, 1)\), would you look for \(y_+\) or \(y_-\). Obviously, we'd look for \(y_+\), and we'd take \(c = 2\), since our solutions are semicircles. Now what if we asked for a solution that went through \((1, 0)\), a solution that is somehow related to that.

This is the interesting thing when people talk about \textit{initial value problems}. How about if someone asked for a solution which \textit{converged} to that point. \textit{That} you could do, since, for example,
\begin{equation}
  y_+(x) = \sqrt{1 - x^2}
\end{equation}
is a solution on \((-1, 1)\) which is continuous on \([-1, 1]\) and \(y_+(x) \to 0\) as \(x \to -1\). We could have said the same thing about \(y_-\) as well.

What if I asked for a solution of equation \ref{morecomp} where \(y(x_0) = y_0\) and \(y(x_1) = y_1\). Can we solve this in general? No, unless by some miracle they're on the same hemisphere, which is highly unlikely. So in general, if we have a \textit{first order} ODE, we can only make one ``request''.

Let's work another example:
\begin{equation}
  y' = y^2
\end{equation}
You can go through the exact same manipulations, and find general solution
\begin{equation}
  y(x) = -\frac{1}{x + c}, c \in \reals
\end{equation}
I have not specified the domain yet: this is just what you get through formal manipulations. This blows up at \(x = -c\), so reasonable choices for intervals on which this is defined are \((-\infty, -c)\) and \((-c, \infty)\). So we get families of solutions
\begin{equation}
\begin{split}
  y_+(x) = -\frac{1}{x + c}, x > -c \\
  y_-(x) = -\frac{1}{x + c}, x < -c \\
  c \in \reals
\end{split}
\end{equation}
We can use these to solve any problem of the form
\begin{equation}
  y' = y^2, y(x_0) = y_0, y_0 \neq 0
\end{equation}
Note we also have the trivial solution \(y = 0\).

Let's look at one more example:
\begin{equation}
  y' = \frac{2x}{9y^2}
\end{equation}
We get general solution
\begin{equation}
  y(x) = \sqrt[3]{3x^2 + c}, c \in \reals
\end{equation}
We have, in some way, just slammed through the first 45 pages of this book. So please don't be surprised if I asked you to read them.

\end{document}
